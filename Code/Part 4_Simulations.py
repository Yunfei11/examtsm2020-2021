"""
If you have written your code in a robust way it should not take too much effort to adjust the following:
When taking the same input parameters as above (part3)
and again modelling the same number of investors with that starting capital,
plot the means for every year in the given dataset (always going from 01/01 to 31/12)
From now on, mixed investors redistribute their budget randomly again when their bond periods end
(again 25-75 bonds vs stocks, 50-50 long term short, …)
what is the impact? What if we switch 75-25 bonds vs stocks
Do you notice a change when multiplying all starting budgets by 10?
"""
# Import packages
import os
import random

import easygui as g
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import yfinance as yf

# A review of bond information
bond = ["ShortTermBond", "LongTermBond"]

# Short term bond information
ShortTermMinimumTerm = 2
ShortTermMinimumAmount = 250
ShortTermAnnualRate = 0.015

# Long term bond information
LongTermMinimumTerm = 5
LongTermMinimumAmount = 1000
LongTermAnnualRate = 0.03

# A review of stock information
stock = ["FDX", "GOOGL", "IBM", "KO", "MS", "NOK", "XOM"]

# Extract date data from yahoo finance
dataset = yf.download('FDX', '2016-09-02', '2021-01-01')
date = list(dataset.index)

# Import datasets (stocks)
FDX = os.path.abspath('..\Data\FDX.csv')
FDXdata = pd.read_csv(FDX, sep=",")

GOOGL = os.path.abspath("..\Data\GOOGL.csv")
GOOGLdata = pd.read_csv(GOOGL, sep=",")

IBM = os.path.abspath("..\Data\IBM.csv")
IBMdata = pd.read_csv(IBM, sep=",")

KO = os.path.abspath("..\Data\KO.csv")
KOdata = pd.read_csv(KO, sep=",")

MS = os.path.abspath("..\Data\MS.csv")
MSdata = pd.read_csv(MS, sep=",")

NOK = os.path.abspath("..\Data\\NOK.csv")
NOKdata = pd.read_csv(NOK, sep=",")

XOM = os.path.abspath("..\Data\XOM.csv")
XOMdata = pd.read_csv(XOM, sep=",")

# Add the date column to each dataset
FDXdata['date'] = date
GOOGLdata["date"] = date
IBMdata["date"] = date
KOdata["date"] = date
MSdata["date"] = date
NOKdata["date"] = date
XOMdata["date"] = date

TotalReturn_Bond = 0

BondReturn = 0
BondReturn_2017 = 0
BondReturn_2018 = 0
BondReturn_2019 = 0
BondReturn_2020 = 0

YearlyAverageReturn = list()
CumulativeAverageReturn = list()
AverageReturn_Stock = list()
CumulativeAverageReturn_Stock = list()

# Assumptions of stocks
start_date_2017 = "2017-01-03"
start_date_2018 = "2018-01-02"
start_date_2019 = "2019-01-02"
start_date_2020 = "2020-01-02"
end_date_2020 = "2020-12-31"

TotalReturn_Stock_2017 = 0
TotalReturn_Stock_2018 = 0
TotalReturn_Stock_2019 = 0
TotalReturn_Stock_2020 = 0
CumulativeTotalReturn_Stock_2017 = 0
CumulativeTotalReturn_Stock_2018 = 0
CumulativeTotalReturn_Stock_2019 = 0
CumulativeTotalReturn_Stock_2020 = 0

# Part 4.1 Plot the means for every year in the given dataset (always going from 01/01 to 31/12)

# Investor input an investor mode
InvestorMode = g.choicebox(msg="Choose your investment mode",
                           choices=["Defensive investor", "Aggressive investor", "Mixed investor"],
                           title="Investor mode")

if InvestorMode == "Defensive investor":
    for i in range(500):
        Budget = 5000
        ShortTermBondCount = 0
        LongTermBondCount = 0
        while Budget >= 1000:  # loop until budget is smaller than 1000
            BondType = np.random.choice(a=bond, size=1, replace=False, p=[0.5, 0.5])
            if BondType == "ShortTermBond":
                Budget = Budget - ShortTermMinimumAmount
                ShortTermBondCount += 1
            elif BondType == "LongTermBond":
                Budget = Budget - LongTermMinimumAmount
                LongTermBondCount += 1
        else:  # if the remaining budget is below 1000, then the investor can only buy short term bond
            amount = int(Budget // ShortTermMinimumAmount)
            ShortTermBondCount = ShortTermBondCount + amount

        # Yearly short term bond return and long term bond return
        if ShortTermBondCount != 0 and LongTermBondCount != 0:
            ShortTermBondValue = ShortTermBondCount * ShortTermMinimumAmount
            ShortTermBondReturn = ((ShortTermBondValue * (1 + ShortTermAnnualRate)) - ShortTermBondValue) / ShortTermBondValue

            LongTermBondValue = LongTermBondCount * LongTermMinimumAmount
            LongTermBondReturn = ((LongTermBondValue * (1 + LongTermAnnualRate)) - LongTermBondValue) / LongTermBondValue

            BondReturn = BondReturn + ShortTermBondReturn + LongTermBondReturn

        # Cumulative yearly short term bond return and long term bond return
        if ShortTermBondCount != 0 and LongTermBondCount != 0:
            ShortTermBondValue = ShortTermBondCount * ShortTermMinimumAmount
            ShortTermBondReturn_2017 = ((ShortTermBondValue * ((1 + ShortTermAnnualRate) ** 1)) - ShortTermBondValue) / ShortTermBondValue
            ShortTermBondReturn_2018 = ((ShortTermBondValue * ((1 + ShortTermAnnualRate) ** 2)) - ShortTermBondValue) / ShortTermBondValue
            ShortTermBondReturn_2019 = ((ShortTermBondValue * ((1 + ShortTermAnnualRate) ** 3)) - ShortTermBondValue) / ShortTermBondValue
            ShortTermBondReturn_2020 = ((ShortTermBondValue * ((1 + ShortTermAnnualRate) ** 4)) - ShortTermBondValue) / ShortTermBondValue

            LongTermBondValue = LongTermBondCount * LongTermMinimumAmount
            LongTermBondReturn_2017 = ((LongTermBondValue * ((1 + LongTermAnnualRate) ** 1)) - LongTermBondValue) / LongTermBondValue
            LongTermBondReturn_2018 = ((LongTermBondValue * ((1 + LongTermAnnualRate) ** 2)) - LongTermBondValue) / LongTermBondValue
            LongTermBondReturn_2019 = ((LongTermBondValue * ((1 + LongTermAnnualRate) ** 3)) - LongTermBondValue) / LongTermBondValue
            LongTermBondReturn_2020 = ((LongTermBondValue * ((1 + LongTermAnnualRate) ** 4)) - LongTermBondValue) / LongTermBondValue

            BondReturn_2017 = BondReturn_2017 + ShortTermBondReturn_2017 + LongTermBondReturn_2017
            BondReturn_2018 = BondReturn_2018 + ShortTermBondReturn_2018 + LongTermBondReturn_2018
            BondReturn_2019 = BondReturn_2019 + ShortTermBondReturn_2019 + LongTermBondReturn_2019
            BondReturn_2020 = BondReturn_2020 + ShortTermBondReturn_2020 + LongTermBondReturn_2020

    # Average return of the 500 defensive investors
    AverageReturn = BondReturn / 500
    for i in range(4):
        YearlyAverageReturn.append(AverageReturn)

    CumulativeAverageReturn.append(BondReturn_2017 / 500)
    CumulativeAverageReturn.append(BondReturn_2018 / 500)
    CumulativeAverageReturn.append(BondReturn_2019 / 500)
    CumulativeAverageReturn.append(BondReturn_2020 / 500)

# Make Yearly returns and Cumulative returns graph
    year = list()
    for i in range(2017, 2021):
        year.append(i)
    plt.figure(figsize=(8,8))
    plt.subplot(2, 1, 1)
    plt.plot(year, YearlyAverageReturn)
    plt.xlabel("Years",fontsize=10)
    plt.ylabel("Returns",fontsize=10)
    plt.title("Yearly Average returns from 2017 to 2020 for defensive investors",fontsize=15)
    plt.grid(True)

    plt.subplot(2, 1, 2)
    plt.plot(year, CumulativeAverageReturn)
    plt.xlabel("Years",fontsize=10)
    plt.ylabel("Returns",fontsize=10)
    plt.title("Cumulative returns from 2017 to 2020 for defensive investors",fontsize=15)
    plt.grid(True)
    plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.5, hspace=0.5)
    plt.show()

elif InvestorMode == "Aggressive investor":
    for i in range(500):
        Budget = 5000
        TotalStockBought_start_2017 = 0
        TotalStockBought_start_2018 = 0
        TotalStockBought_start_2019 = 0
        TotalStockBought_start_2020 = 0
        TotalStockBought_end_2020 = 0
        while Budget >= 100:
            # Randomly choose stock
            StockType = np.random.choice(a=stock, size=1, replace=False, p=None)
            if StockType == "FDX":
                price_2017 = float(FDXdata.loc[FDXdata["date"] == start_date_2017, "high"].iloc[0])
                price_2018 = float(FDXdata.loc[FDXdata["date"] == start_date_2018, "high"].iloc[0])
                price_2019 = float(FDXdata.loc[FDXdata["date"] == start_date_2019, "high"].iloc[0])
                price_2020 = float(FDXdata.loc[FDXdata["date"] == start_date_2020, "high"].iloc[0])
                price_end_2020 = float(FDXdata.loc[FDXdata["date"] == end_date_2020, "high"].iloc[0])
                amount = (Budget // price_2017)
                StockBought = random.randint(0, amount)
                Budget = Budget - (StockBought * price_2017)
                TotalStockBought_start_2017 = TotalStockBought_start_2017 + (StockBought * price_2017)
                TotalStockBought_start_2018 = TotalStockBought_start_2018 + (StockBought * price_2018)
                TotalStockBought_start_2019 = TotalStockBought_start_2019 + (StockBought * price_2019)
                TotalStockBought_start_2020 = TotalStockBought_start_2020 + (StockBought * price_2020)
                TotalStockBought_end_2020 = TotalStockBought_end_2020 + (StockBought * price_end_2020)
            elif StockType == "GOOGL":
                price_2017 = float(GOOGLdata.loc[GOOGLdata["date"] == start_date_2017, "high"].iloc[0])
                price_2018 = float(GOOGLdata.loc[GOOGLdata["date"] == start_date_2018, "high"].iloc[0])
                price_2019 = float(GOOGLdata.loc[GOOGLdata["date"] == start_date_2019, "high"].iloc[0])
                price_2020 = float(GOOGLdata.loc[GOOGLdata["date"] == start_date_2020, "high"].iloc[0])
                price_end_2020 = float(GOOGLdata.loc[GOOGLdata["date"] == end_date_2020, "high"].iloc[0])

                amount = (Budget // price_2017)
                StockBought = random.randint(0, amount)
                Budget = Budget - (StockBought * price_2017)
                TotalStockBought_start_2017 = TotalStockBought_start_2017 + (StockBought * price_2017)
                TotalStockBought_start_2018 = TotalStockBought_start_2018 + (StockBought * price_2018)
                TotalStockBought_start_2019 = TotalStockBought_start_2019 + (StockBought * price_2019)
                TotalStockBought_start_2020 = TotalStockBought_start_2020 + (StockBought * price_2020)
                TotalStockBought_end_2020 = TotalStockBought_end_2020 + (StockBought * price_end_2020)
            elif StockType == "IBM":
                price_2017 = float(IBMdata.loc[IBMdata["date"] == start_date_2017, "high"].iloc[0])
                price_2018 = float(IBMdata.loc[IBMdata["date"] == start_date_2018, "high"].iloc[0])
                price_2019 = float(IBMdata.loc[IBMdata["date"] == start_date_2019, "high"].iloc[0])
                price_2020 = float(IBMdata.loc[IBMdata["date"] == start_date_2020, "high"].iloc[0])
                price_end_2020 = float(IBMdata.loc[IBMdata["date"] == end_date_2020, "high"].iloc[0])
                amount = (Budget // price_2017)
                StockBought = random.randint(0, amount)
                Budget = Budget - (StockBought * price_2017)
                TotalStockBought_start_2017 = TotalStockBought_start_2017 + (StockBought * price_2017)
                TotalStockBought_start_2018 = TotalStockBought_start_2018 + (StockBought * price_2018)
                TotalStockBought_start_2019 = TotalStockBought_start_2019 + (StockBought * price_2019)
                TotalStockBought_start_2020 = TotalStockBought_start_2020 + (StockBought * price_2020)
                TotalStockBought_end_2020 = TotalStockBought_end_2020 + (StockBought * price_end_2020)
            elif StockType == "KO":
                price_2017 = float(KOdata.loc[KOdata["date"] == start_date_2017, "high"].iloc[0])
                price_2018 = float(KOdata.loc[KOdata["date"] == start_date_2018, "high"].iloc[0])
                price_2019 = float(KOdata.loc[KOdata["date"] == start_date_2019, "high"].iloc[0])
                price_2020 = float(KOdata.loc[KOdata["date"] == start_date_2020, "high"].iloc[0])
                price_end_2020 = float(KOdata.loc[KOdata["date"] == end_date_2020, "high"].iloc[0])
                amount = (Budget // price_2017)
                StockBought = random.randint(0, amount)
                Budget = Budget - (StockBought * price_2017)
                TotalStockBought_start_2017 = TotalStockBought_start_2017 + (StockBought * price_2017)
                TotalStockBought_start_2018 = TotalStockBought_start_2018 + (StockBought * price_2018)
                TotalStockBought_start_2019 = TotalStockBought_start_2019 + (StockBought * price_2019)
                TotalStockBought_start_2020 = TotalStockBought_start_2020 + (StockBought * price_2020)
                TotalStockBought_end_2020 = TotalStockBought_end_2020 + (StockBought * price_end_2020)
            elif StockType == "MS":
                price_2017 = float(MSdata.loc[MSdata["date"] == start_date_2017, "high"].iloc[0])
                price_2018 = float(MSdata.loc[MSdata["date"] == start_date_2018, "high"].iloc[0])
                price_2019 = float(MSdata.loc[MSdata["date"] == start_date_2019, "high"].iloc[0])
                price_2020 = float(MSdata.loc[MSdata["date"] == start_date_2020, "high"].iloc[0])
                price_end_2020 = float(MSdata.loc[MSdata["date"] == end_date_2020, "high"].iloc[0])
                amount = (Budget // price_2017)
                StockBought = random.randint(0, amount)
                Budget = Budget - (StockBought * price_2017)
                TotalStockBought_start_2017 = TotalStockBought_start_2017 + (StockBought * price_2017)
                TotalStockBought_start_2018 = TotalStockBought_start_2018 + (StockBought * price_2018)
                TotalStockBought_start_2019 = TotalStockBought_start_2019 + (StockBought * price_2019)
                TotalStockBought_start_2020 = TotalStockBought_start_2020 + (StockBought * price_2020)
                TotalStockBought_end_2020 = TotalStockBought_end_2020 + (StockBought * price_end_2020)
            elif StockType == "NOK":
                price_2017 = float(NOKdata.loc[NOKdata["date"] == start_date_2017, "high"].iloc[0])
                price_2018 = float(NOKdata.loc[NOKdata["date"] == start_date_2018, "high"].iloc[0])
                price_2019 = float(NOKdata.loc[NOKdata["date"] == start_date_2019, "high"].iloc[0])
                price_2020 = float(NOKdata.loc[NOKdata["date"] == start_date_2020, "high"].iloc[0])
                price_end_2020 = float(NOKdata.loc[NOKdata["date"] == end_date_2020, "high"].iloc[0])
                amount = (Budget // price_2017)
                StockBought = random.randint(0, amount)
                Budget = Budget - (StockBought * price_2017)
                TotalStockBought_start_2017 = TotalStockBought_start_2017 + (StockBought * price_2017)
                TotalStockBought_start_2018 = TotalStockBought_start_2018 + (StockBought * price_2018)
                TotalStockBought_start_2019 = TotalStockBought_start_2019 + (StockBought * price_2019)
                TotalStockBought_start_2020 = TotalStockBought_start_2020 + (StockBought * price_2020)
                TotalStockBought_end_2020 = TotalStockBought_end_2020 + (StockBought * price_end_2020)
            elif StockType == "XOM":
                price_2017 = float(XOMdata.loc[XOMdata["date"] == start_date_2017, "high"].iloc[0])
                price_2018 = float(XOMdata.loc[XOMdata["date"] == start_date_2018, "high"].iloc[0])
                price_2019 = float(XOMdata.loc[XOMdata["date"] == start_date_2019, "high"].iloc[0])
                price_2020 = float(XOMdata.loc[XOMdata["date"] == start_date_2020, "high"].iloc[0])
                price_end_2020 = float(XOMdata.loc[XOMdata["date"] == end_date_2020, "high"].iloc[0])
                amount = (Budget // price_2017)
                StockBought = random.randint(0, amount)
                Budget = Budget - (StockBought * price_2017)
                TotalStockBought_start_2017 = TotalStockBought_start_2017 + (StockBought * price_2017)
                TotalStockBought_start_2018 = TotalStockBought_start_2018 + (StockBought * price_2018)
                TotalStockBought_start_2019 = TotalStockBought_start_2019 + (StockBought * price_2019)
                TotalStockBought_start_2020 = TotalStockBought_start_2020 + (StockBought * price_2020)
                TotalStockBought_end_2020 = TotalStockBought_end_2020 + (StockBought * price_end_2020)

        # Yearly return for aggressive investor
        if TotalStockBought_start_2017 != 0:
            TotalReturn_2017 = (TotalStockBought_start_2018 - TotalStockBought_start_2017) / TotalStockBought_start_2017
            TotalReturn_Stock_2017 = TotalReturn_Stock_2017 + TotalReturn_2017
            TotalReturn_2018 = (TotalStockBought_start_2019 - TotalStockBought_start_2018) / TotalStockBought_start_2018
            TotalReturn_Stock_2018 = TotalReturn_Stock_2018 + TotalReturn_2018
            TotalReturn_2019 = (TotalStockBought_start_2020 - TotalStockBought_start_2019) / TotalStockBought_start_2019
            TotalReturn_Stock_2019 = TotalReturn_Stock_2019 + TotalReturn_2019
            TotalReturn_2020 = (TotalStockBought_end_2020 - TotalStockBought_start_2020) / TotalStockBought_start_2020
            TotalReturn_Stock_2020 = TotalReturn_Stock_2020 + TotalReturn_2020

            CumulativeTotalReturn_2017 = (TotalStockBought_start_2018 - TotalStockBought_start_2017) / TotalStockBought_start_2017
            CumulativeTotalReturn_Stock_2017 = CumulativeTotalReturn_Stock_2017 + CumulativeTotalReturn_2017
            CumulativeTotalReturn_2018 = (TotalStockBought_start_2019 - TotalStockBought_start_2017) / TotalStockBought_start_2017
            CumulativeTotalReturn_Stock_2018 = CumulativeTotalReturn_Stock_2018 + CumulativeTotalReturn_2018
            CumulativeTotalReturn_2019 = (TotalStockBought_start_2020 - TotalStockBought_start_2017) / TotalStockBought_start_2017
            CumulativeTotalReturn_Stock_2019 = CumulativeTotalReturn_Stock_2019 + CumulativeTotalReturn_2019
            CumulativeTotalReturn_2020 = (TotalStockBought_end_2020 - TotalStockBought_start_2017) / TotalStockBought_start_2017
            CumulativeTotalReturn_Stock_2020 = CumulativeTotalReturn_Stock_2020 + CumulativeTotalReturn_2020

    # Average return of the 500 aggressive investors


    AverageReturn_Stock.append(TotalReturn_Stock_2017 / 500)
    AverageReturn_Stock.append(TotalReturn_Stock_2018 / 500)
    AverageReturn_Stock.append(TotalReturn_Stock_2018 / 500)
    AverageReturn_Stock.append(TotalReturn_Stock_2020 / 500)

    CumulativeAverageReturn_Stock.append(CumulativeTotalReturn_Stock_2017 / 500)
    CumulativeAverageReturn_Stock.append(CumulativeTotalReturn_Stock_2018 / 500)
    CumulativeAverageReturn_Stock.append(CumulativeTotalReturn_Stock_2019 / 500)
    CumulativeAverageReturn_Stock.append(CumulativeTotalReturn_Stock_2020 / 500)


#make the graph
    year=[]
    for i in range(2017, 2021):
        year.append(i)
    plt.figure(figsize=(8, 8))
    plt.plot(year, AverageReturn_Stock, label='Yearly Average returns', color='r')
    plt.plot(year, CumulativeAverageReturn_Stock, label='Cumulative returns',color = 'b')
    plt.xlabel("Years", fontsize=10)
    plt.ylabel("Returns", fontsize=10)
    plt.title('Yearly Average Return and Cumulative Returns from 2017 to 2020 for aggressive investors',fontsize=12)
    plt.grid(True)
    plt.legend(loc='upper right')
    plt.show()

elif InvestorMode == "Mixed investor":
    for i in range(500):
        Budget = 5000
        ShortTermBondCount = 0
        LongTermBondCount = 0
        TotalStockBought_start_2017 = 0
        TotalStockBought_start_2018 = 0
        TotalStockBought_start_2019 = 0
        TotalStockBought_start_2020 = 0
        TotalStockBought_end_2020 = 0
        while Budget >= 250:  # loop until budget is smaller than 250
            # 25-75 chance of buying a bond or stocks
            InvestmentType = np.random.choice(["Bond", "Stock"], size=1, replace=False, p=[0.25, 0.75])
            if InvestmentType == "Bond":
                BondType = np.random.choice(a=["ShortTermBond", "LongTermBond"], size=1, replace=False, p=[0.5, 0.5])
                if BondType == "ShortTermBond":
                    Budget = Budget - ShortTermMinimumAmount
                    ShortTermBondCount += 1
                elif BondType == "LongTermBond":
                    Budget = Budget - LongTermMinimumAmount
                    LongTermBondCount += 1
            elif InvestmentType == "Stock":
                StockType = np.random.choice(a=stock, size=1, replace=False, p=None)
                if StockType == "FDX":
                    price_2017 = float(FDXdata.loc[FDXdata["date"] == start_date_2017, "high"].iloc[0])
                    price_2018 = float(FDXdata.loc[FDXdata["date"] == start_date_2018, "high"].iloc[0])
                    price_2019 = float(FDXdata.loc[FDXdata["date"] == start_date_2019, "high"].iloc[0])
                    price_2020 = float(FDXdata.loc[FDXdata["date"] == start_date_2020, "high"].iloc[0])
                    price_end_2020 = float(FDXdata.loc[FDXdata["date"] == end_date_2020, "high"].iloc[0])
                    amount = (Budget // price_2017)
                    StockBought = random.randint(0, amount)
                    Budget = Budget - (StockBought * price_2017)
                    TotalStockBought_start_2017 = TotalStockBought_start_2017 + (StockBought * price_2017)
                    TotalStockBought_start_2018 = TotalStockBought_start_2018 + (StockBought * price_2018)
                    TotalStockBought_start_2019 = TotalStockBought_start_2019 + (StockBought * price_2019)
                    TotalStockBought_start_2020 = TotalStockBought_start_2020 + (StockBought * price_2020)
                    TotalStockBought_end_2020 = TotalStockBought_end_2020 + (StockBought * price_end_2020)
                elif StockType == "GOOGL":
                    price_2017 = float(GOOGLdata.loc[GOOGLdata["date"] == start_date_2017, "high"].iloc[0])
                    price_2018 = float(GOOGLdata.loc[GOOGLdata["date"] == start_date_2018, "high"].iloc[0])
                    price_2019 = float(GOOGLdata.loc[GOOGLdata["date"] == start_date_2019, "high"].iloc[0])
                    price_2020 = float(GOOGLdata.loc[GOOGLdata["date"] == start_date_2020, "high"].iloc[0])
                    price_end_2020 = float(GOOGLdata.loc[GOOGLdata["date"] == end_date_2020, "high"].iloc[0])
                    amount = (Budget // price_2017)
                    StockBought = random.randint(0, amount)
                    Budget = Budget - (StockBought * price_2017)
                    TotalStockBought_start_2017 = TotalStockBought_start_2017 + (StockBought * price_2017)
                    TotalStockBought_start_2018 = TotalStockBought_start_2018 + (StockBought * price_2018)
                    TotalStockBought_start_2019 = TotalStockBought_start_2019 + (StockBought * price_2019)
                    TotalStockBought_start_2020 = TotalStockBought_start_2020 + (StockBought * price_2020)
                    TotalStockBought_end_2020 = TotalStockBought_end_2020 + (StockBought * price_end_2020)
                elif StockType == "IBM":
                    price_2017 = float(IBMdata.loc[IBMdata["date"] == start_date_2017, "high"].iloc[0])
                    price_2018 = float(IBMdata.loc[IBMdata["date"] == start_date_2018, "high"].iloc[0])
                    price_2019 = float(IBMdata.loc[IBMdata["date"] == start_date_2019, "high"].iloc[0])
                    price_2020 = float(IBMdata.loc[IBMdata["date"] == start_date_2020, "high"].iloc[0])
                    price_end_2020 = float(IBMdata.loc[IBMdata["date"] == end_date_2020, "high"].iloc[0])
                    amount = (Budget // price_2017)
                    StockBought = random.randint(0, amount)
                    Budget = Budget - (StockBought * price_2017)
                    TotalStockBought_start_2017 = TotalStockBought_start_2017 + (StockBought * price_2017)
                    TotalStockBought_start_2018 = TotalStockBought_start_2018 + (StockBought * price_2018)
                    TotalStockBought_start_2019 = TotalStockBought_start_2019 + (StockBought * price_2019)
                    TotalStockBought_start_2020 = TotalStockBought_start_2020 + (StockBought * price_2020)
                    TotalStockBought_end_2020 = TotalStockBought_end_2020 + (StockBought * price_end_2020)
                elif StockType == "KO":
                    price_2017 = float(KOdata.loc[KOdata["date"] == start_date_2017, "high"].iloc[0])
                    price_2018 = float(KOdata.loc[KOdata["date"] == start_date_2018, "high"].iloc[0])
                    price_2019 = float(KOdata.loc[KOdata["date"] == start_date_2019, "high"].iloc[0])
                    price_2020 = float(KOdata.loc[KOdata["date"] == start_date_2020, "high"].iloc[0])
                    price_end_2020 = float(KOdata.loc[KOdata["date"] == end_date_2020, "high"].iloc[0])
                    amount = (Budget // price_2017)
                    StockBought = random.randint(0, amount)
                    Budget = Budget - (StockBought * price_2017)
                    TotalStockBought_start_2017 = TotalStockBought_start_2017 + (StockBought * price_2017)
                    TotalStockBought_start_2018 = TotalStockBought_start_2018 + (StockBought * price_2018)
                    TotalStockBought_start_2019 = TotalStockBought_start_2019 + (StockBought * price_2019)
                    TotalStockBought_start_2020 = TotalStockBought_start_2020 + (StockBought * price_2020)
                    TotalStockBought_end_2020 = TotalStockBought_end_2020 + (StockBought * price_end_2020)
                elif StockType == "MS":
                    price_2017 = float(MSdata.loc[MSdata["date"] == start_date_2017, "high"].iloc[0])
                    price_2018 = float(MSdata.loc[MSdata["date"] == start_date_2018, "high"].iloc[0])
                    price_2019 = float(MSdata.loc[MSdata["date"] == start_date_2019, "high"].iloc[0])
                    price_2020 = float(MSdata.loc[MSdata["date"] == start_date_2020, "high"].iloc[0])
                    price_end_2020 = float(MSdata.loc[MSdata["date"] == end_date_2020, "high"].iloc[0])
                    amount = (Budget // price_2017)
                    StockBought = random.randint(0, amount)
                    Budget = Budget - (StockBought * price_2017)
                    TotalStockBought_start_2017 = TotalStockBought_start_2017 + (StockBought * price_2017)
                    TotalStockBought_start_2018 = TotalStockBought_start_2018 + (StockBought * price_2018)
                    TotalStockBought_start_2019 = TotalStockBought_start_2019 + (StockBought * price_2019)
                    TotalStockBought_start_2020 = TotalStockBought_start_2020 + (StockBought * price_2020)
                    TotalStockBought_end_2020 = TotalStockBought_end_2020 + (StockBought * price_end_2020)
                elif StockType == "NOK":
                    price_2017 = float(NOKdata.loc[NOKdata["date"] == start_date_2017, "high"].iloc[0])
                    price_2018 = float(NOKdata.loc[NOKdata["date"] == start_date_2018, "high"].iloc[0])
                    price_2019 = float(NOKdata.loc[NOKdata["date"] == start_date_2019, "high"].iloc[0])
                    price_2020 = float(NOKdata.loc[NOKdata["date"] == start_date_2020, "high"].iloc[0])
                    price_end_2020 = float(NOKdata.loc[NOKdata["date"] == end_date_2020, "high"].iloc[0])
                    amount = (Budget // price_2017)
                    StockBought = random.randint(0, amount)
                    Budget = Budget - (StockBought * price_2017)
                    TotalStockBought_start_2017 = TotalStockBought_start_2017 + (StockBought * price_2017)
                    TotalStockBought_start_2018 = TotalStockBought_start_2018 + (StockBought * price_2018)
                    TotalStockBought_start_2019 = TotalStockBought_start_2019 + (StockBought * price_2019)
                    TotalStockBought_start_2020 = TotalStockBought_start_2020 + (StockBought * price_2020)
                    TotalStockBought_end_2020 = TotalStockBought_end_2020 + (StockBought * price_end_2020)
                elif StockType == "XOM":
                    price_2017 = float(XOMdata.loc[XOMdata["date"] == start_date_2017, "high"].iloc[0])
                    price_2018 = float(XOMdata.loc[XOMdata["date"] == start_date_2018, "high"].iloc[0])
                    price_2019 = float(XOMdata.loc[XOMdata["date"] == start_date_2019, "high"].iloc[0])
                    price_2020 = float(XOMdata.loc[XOMdata["date"] == start_date_2020, "high"].iloc[0])
                    price_end_2020 = float(XOMdata.loc[XOMdata["date"] == end_date_2020, "high"].iloc[0])
                    amount = (Budget // price_2017)
                    StockBought = random.randint(0, amount)
                    Budget = Budget - (StockBought * price_2017)
                    TotalStockBought_start_2017 = TotalStockBought_start_2017 + (StockBought * price_2017)
                    TotalStockBought_start_2018 = TotalStockBought_start_2018 + (StockBought * price_2018)
                    TotalStockBought_start_2019 = TotalStockBought_start_2019 + (StockBought * price_2019)
                    TotalStockBought_start_2020 = TotalStockBought_start_2020 + (StockBought * price_2020)
                    TotalStockBought_end_2020 = TotalStockBought_end_2020 + (StockBought * price_end_2020)

        # Yearly short term bond return and long term bond return
        if ShortTermBondCount != 0 and LongTermBondCount != 0:
            ShortTermBondValue = ShortTermBondCount * ShortTermMinimumAmount
            ShortTermBondReturn = ((ShortTermBondValue * (1 + ShortTermAnnualRate)) - ShortTermBondValue) / ShortTermBondValue

            LongTermBondValue = LongTermBondCount * LongTermMinimumAmount
            LongTermBondReturn = ((LongTermBondValue * (1 + LongTermAnnualRate)) - LongTermBondValue) / LongTermBondValue

            BondReturn = BondReturn + ShortTermBondReturn + LongTermBondReturn

        # Cumulative yearly short term bond return and long term bond return
        if ShortTermBondCount != 0 and LongTermBondCount != 0:
            ShortTermBondValue = ShortTermBondCount * ShortTermMinimumAmount
            ShortTermBondReturn_2017 = ((ShortTermBondValue * ((1 + ShortTermAnnualRate) ** 1)) - ShortTermBondValue) / ShortTermBondValue
            ShortTermBondReturn_2018 = ((ShortTermBondValue * ((1 + ShortTermAnnualRate) ** 2)) - ShortTermBondValue) / ShortTermBondValue
            ShortTermBondReturn_2019 = ((ShortTermBondValue * ((1 + ShortTermAnnualRate) ** 3)) - ShortTermBondValue) / ShortTermBondValue
            ShortTermBondReturn_2020 = ((ShortTermBondValue * ((1 + ShortTermAnnualRate) ** 4)) - ShortTermBondValue) / ShortTermBondValue

            LongTermBondValue = LongTermBondCount * LongTermMinimumAmount
            LongTermBondReturn_2017 = ((LongTermBondValue * ((1 + LongTermAnnualRate) ** 1)) - LongTermBondValue) / LongTermBondValue
            LongTermBondReturn_2018 = ((LongTermBondValue * ((1 + LongTermAnnualRate) ** 2)) - LongTermBondValue) / LongTermBondValue
            LongTermBondReturn_2019 = ((LongTermBondValue * ((1 + LongTermAnnualRate) ** 3)) - LongTermBondValue) / LongTermBondValue
            LongTermBondReturn_2020 = ((LongTermBondValue * ((1 + LongTermAnnualRate) ** 4)) - LongTermBondValue) / LongTermBondValue

            BondReturn_2017 = BondReturn_2017 + ShortTermBondReturn_2017 + LongTermBondReturn_2017
            BondReturn_2018 = BondReturn_2018 + ShortTermBondReturn_2018 + LongTermBondReturn_2018
            BondReturn_2019 = BondReturn_2019 + ShortTermBondReturn_2019 + LongTermBondReturn_2019
            BondReturn_2020 = BondReturn_2020 + ShortTermBondReturn_2020 + LongTermBondReturn_2020

        # Stock return
        if TotalStockBought_start_2017 != 0:
            TotalReturn_2017 = (TotalStockBought_start_2018 - TotalStockBought_start_2017) / TotalStockBought_start_2017
            TotalReturn_Stock_2017 = TotalReturn_Stock_2017 + TotalReturn_2017
            TotalReturn_2018 = (TotalStockBought_start_2019 - TotalStockBought_start_2018) / TotalStockBought_start_2018
            TotalReturn_Stock_2018 = TotalReturn_Stock_2018 + TotalReturn_2018
            TotalReturn_2019 = (TotalStockBought_start_2020 - TotalStockBought_start_2019) / TotalStockBought_start_2019
            TotalReturn_Stock_2019 = TotalReturn_Stock_2019 + TotalReturn_2019
            TotalReturn_2020 = (TotalStockBought_end_2020 - TotalStockBought_start_2020) / TotalStockBought_start_2020
            TotalReturn_Stock_2020 = TotalReturn_Stock_2020 + TotalReturn_2020

            CumulativeTotalReturn_2017 = (TotalStockBought_start_2018 - TotalStockBought_start_2017) / TotalStockBought_start_2017
            CumulativeTotalReturn_Stock_2017 = CumulativeTotalReturn_Stock_2017 + CumulativeTotalReturn_2017
            CumulativeTotalReturn_2018 = (TotalStockBought_start_2019 - TotalStockBought_start_2017) / TotalStockBought_start_2017
            CumulativeTotalReturn_Stock_2018 = CumulativeTotalReturn_Stock_2018 + CumulativeTotalReturn_2018
            CumulativeTotalReturn_2019 = (TotalStockBought_start_2020 - TotalStockBought_start_2017) / TotalStockBought_start_2017
            CumulativeTotalReturn_Stock_2019 = CumulativeTotalReturn_Stock_2019 + CumulativeTotalReturn_2019
            CumulativeTotalReturn_2020 = (TotalStockBought_end_2020 - TotalStockBought_start_2017) / TotalStockBought_start_2017
            CumulativeTotalReturn_Stock_2020 = CumulativeTotalReturn_Stock_2020 + CumulativeTotalReturn_2020

    # Total return for mixed investor
    TotalReturn_Mixed_2017 = BondReturn + TotalReturn_Stock_2017
    TotalReturn_Mixed_2018 = BondReturn + TotalReturn_Stock_2018
    TotalReturn_Mixed_2019 = BondReturn + TotalReturn_Stock_2019
    TotalReturn_Mixed_2020 = BondReturn + TotalReturn_Stock_2020

    # Average return of the 500 mixed investors
    YearlyAverageReturn.append(TotalReturn_Mixed_2017 / 500)
    YearlyAverageReturn.append(TotalReturn_Mixed_2018 / 500)
    YearlyAverageReturn.append(TotalReturn_Mixed_2019 / 500)
    YearlyAverageReturn.append(TotalReturn_Mixed_2020 / 500)

    # Cumulative total return
    CumulativeTotalReturn_Mixed_2017 = BondReturn_2017 + CumulativeTotalReturn_Stock_2017
    CumulativeTotalReturn_Mixed_2018 = BondReturn_2018 + CumulativeTotalReturn_Stock_2018
    CumulativeTotalReturn_Mixed_2019 = BondReturn_2019 + CumulativeTotalReturn_Stock_2019
    CumulativeTotalReturn_Mixed_2020 = BondReturn_2020 + CumulativeTotalReturn_Stock_2020

    CumulativeAverageReturn.append(CumulativeTotalReturn_Mixed_2017/500)
    CumulativeAverageReturn.append(CumulativeTotalReturn_Mixed_2018/500)
    CumulativeAverageReturn.append(CumulativeTotalReturn_Mixed_2019/500)
    CumulativeAverageReturn.append(CumulativeTotalReturn_Mixed_2020/500)

    # make the graph
    year = []
    for i in range(2017, 2021):
        year.append(i)
    plt.figure(figsize=(8, 8))
    plt.plot(year, YearlyAverageReturn, label='Yearly Average returns', color='r')
    plt.plot(year, CumulativeAverageReturn, label='Cumulative returns', color='b')
    plt.xlabel("Years", fontsize=10)
    plt.ylabel("Returns", fontsize=10)
    plt.title('Yearly Average Return and Cumulative Returns from 2017 to 2020 for mixed investors', fontsize=12)
    plt.grid(True)
    plt.legend(loc='upper right')
    plt.show()



# Part 4.2
# Mixed investors redistribute their budget randomly again when their bond periods end
# (again 25-75 bonds vs stocks, 50-50 long term short, …), what is the impact?

# Assumptions of stocks
start_date = "2016-09-01" # the date at which the investor will buy the stock
end_date = "2020-12-31" # the date at which the investor will sell the stock
TotalReturn_Stock = 0
TotalReturn_Bond = 0

for i in range(500):
    Budget = 5000
    ShortTermBondCount = 0
    LongTermBondCount = 0
    TotalStockBought_start = 0
    TotalStockBought_end = 0
    TotalReturn_ShortTermBond = 0
    ShortTermBondChance = 0
    while Budget >= 250:  # loop until budget is smaller than 250
        # 25-75 chance of buying a bond or stocks
        InvestmentType = np.random.choice(["Bond", "Stock"], size=1, replace=False, p=[0.25, 0.75])
        if InvestmentType == "Bond":
            BondType = np.random.choice(a=["ShortTermBond", "LongTermBond"], size=1, replace=False, p=[0.5, 0.5])
            if BondType == "ShortTermBond":
                Budget = Budget - ShortTermMinimumAmount
                ShortTermBondCount += 1
            elif BondType == "LongTermBond":
                Budget = Budget - LongTermMinimumAmount
                LongTermBondCount += 1
        elif InvestmentType == "Stock":
            StockType = np.random.choice(a=stock, size=1, replace=False, p=None)
            if StockType == "FDX":
                price_start = float(FDXdata.loc[FDXdata["date"] == start_date, "high"].iloc[0])
                amount = (Budget // price_start)
                StockBought = random.randint(0, amount)
                Budget = Budget - (StockBought * price_start)
                TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                price_end = float(FDXdata.loc[FDXdata["date"] == end_date, "high"].iloc[0])
                TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
            elif StockType == "GOOGL":
                price_start = float(GOOGLdata.loc[GOOGLdata["date"] == start_date, "high"].iloc[0])
                amount = (Budget // price_start)
                StockBought = random.randint(0, amount)
                Budget = Budget - (StockBought * price_start)
                TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                price_end = float(GOOGLdata.loc[GOOGLdata["date"] == end_date, "high"].iloc[0])
                TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
            elif StockType == "IBM":
                price_start = float(IBMdata.loc[IBMdata["date"] == start_date, "high"].iloc[0])
                amount = (Budget // price_start)
                StockBought = random.randint(0, amount)
                Budget = Budget - (StockBought * price_start)
                TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                price_end = float(IBMdata.loc[IBMdata["date"] == end_date, "high"].iloc[0])
                TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
            elif StockType == "KO":
                price_start = float(KOdata.loc[KOdata["date"] == start_date, "high"].iloc[0])
                amount = (Budget // price_start)
                StockBought = random.randint(0, amount)
                Budget = Budget - (StockBought * price_start)
                TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                price_end = float(KOdata.loc[KOdata["date"] == end_date, "high"].iloc[0])
                TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
            elif StockType == "MS":
                price_start = float(MSdata.loc[MSdata["date"] == start_date, "high"].iloc[0])
                amount = (Budget // price_start)
                StockBought = random.randint(0, amount)
                Budget = Budget - (StockBought * price_start)
                TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                price_end = float(MSdata.loc[MSdata["date"] == end_date, "high"].iloc[0])
                TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
            elif StockType == "NOK":
                price_start = float(NOKdata.loc[NOKdata["date"] == start_date, "high"].iloc[0])
                amount = (Budget // price_start)
                StockBought = random.randint(0, amount)
                Budget = Budget - (StockBought * price_start)
                TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                price_end = float(NOKdata.loc[NOKdata["date"] == end_date, "high"].iloc[0])
                TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
            elif StockType == "XOM":
                price_start = float(XOMdata.loc[XOMdata["date"] == start_date, "high"].iloc[0])
                amount = (Budget // price_start)
                StockBought = random.randint(0, amount)
                Budget = Budget - (StockBought * price_start)
                TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                price_end = float(XOMdata.loc[XOMdata["date"] == end_date, "high"].iloc[0])
                TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)

    # Short term bond value after 2 years (2016-09-01 -> 2018-08-31) and (2018-09-01 -> 2020-08-31)
    if ShortTermBondChance <= 2 and ShortTermBondCount != 0:
        # The investor gets back the investment of short term bond after 2 years
        ShortTermBond = ShortTermBondCount * ShortTermMinimumAmount * ((1 + ShortTermAnnualRate) ** ShortTermMinimumTerm)
        Budget = Budget + ShortTermBond # Add the investment return into the budget
        ShortTermBondChance += 1 # As short term bond has a minimum term of 2 years, thus it can only be invested twice within the period 2016-09-01 to 2021-01-01
        # Calculate the return of the existing short term bond
        ShortTermBondValue = ShortTermBondCount * ShortTermMinimumAmount
        ShortTermBondInterest = (1 + ShortTermAnnualRate) ** ShortTermMinimumTerm
        ShortTermBondReturn = ((ShortTermBondValue * ShortTermBondInterest) - ShortTermBondValue) / ShortTermBondValue
        TotalReturn_ShortTermBond = TotalReturn_ShortTermBond + ShortTermBondReturn
        # Re-initiate the count variable for short term bond
        ShortTermBondCount = 0

    # Long term bond return
    if LongTermBondCount != 0:
        LongTermBondValue = LongTermBondCount * LongTermMinimumAmount
        LongTermBondInterest = (1 + LongTermAnnualRate) ** LongTermMinimumTerm
        LongTermBondReturn = ((LongTermBondValue * LongTermBondInterest) - LongTermBondValue) / LongTermBondValue

    # Total bond return
    if ShortTermBondCount != 0 and LongTermBondCount != 0:
        Return_Bond = TotalReturn_ShortTermBond + LongTermBondReturn
        TotalReturn_Bond = TotalReturn_Bond + Return_Bond

    # Stock return
    if TotalStockBought_start != 0 and TotalStockBought_end != 0:
        Return_Stock = (TotalStockBought_end - TotalStockBought_start) / TotalStockBought_start
        TotalReturn_Stock = TotalReturn_Stock + Return_Stock

# Total return for mixed investor
TotalReturn_Mixed = TotalReturn_Bond + TotalReturn_Stock
AverageReturn_Mixed = TotalReturn_Mixed / 500
print("The average return of the 500 mixed investors is {:.2%}".format(AverageReturn_Mixed))

"""
Mixed investors redistribute their budget randomly again when their bond periods end,
which means that during the period 2016.09.01-2021.01.01, only the short term bond will be redistributed.
We modified the code above, and we notice that by repeating hundreds times of the loop, 
the Mixed investor total return mainly lays in the interval of 23%-34.5% (0.23-0.345), 
and the most frequent total return is around 29% (0.29).

The results of return interval is more narrow than the return of the portfolio 
that the Mixed investors do not redistribute their budget (27%-40% (0.27-0.4)).
In addition, the most frequent total return of redistributed investors 29% (0.29) is also 
lower than that not redistributed (33% (0.33)).
We believe that, as they redistribute their budget randomly again when their bond periods end,
they will invest more on the stock. Then, it could increase their portfolio risk and reduce their profit.
"""

#Part 4.3 What if we switch 75-25 bonds vs stocks ?
"""
We change the InvestmentType probability to p=[0.75, 0.25].
The code of this part is totally same with above except InvestmentType probability to p=[0.75, 0.25],
as we want to keep the interface simple and clean, we remove the repetitive code, but leave the conclusion only.

Conclusion: 
We did that code in another python file, and the results show that
the Mixed investor (75-25 bonds vs stocks) total return mainly lays in the interval of 18%-30% (0.18-0.3), 
and the most frequent total return is around 25% (0.25).
As bonds are considered to be a safer investment (low risk, low returns),
returns become more stable as the investors invest more in the bonds (75%).
Therefore, both the return range and the most frequent total return moves downward.
"""

#part 4.4 If all starting budgets multiply by 10?
'''
We change the parameter of the budget from 5000 to 50000(Budget = 50000).
Since it seeks to the influence of increasing budget for all investors mode type,
The code of this part is same with part 3 except budget changing from 5000 to 50000(Budget = 50000),
as we want to keep the interface simple and clean, we remove the repetitive code, but leave the conclusion only.

Conclusion:
We did the code in another python file, and the results show that
As the budget increases to 50000, there's more possibility of equal investment opportunities on bond. 
Therefore,the return should be as the average of short term bond and long term bond.
As observed before,the return would follow as normal distribution.
For aggressive investors, with the higher budget, the normal distribution tends to leptokurtic distribution,
which means that it would generate more frequent and more concentrated on the mean return.
Similarly, for investors that invest mixed bond and stock investment, the return interval would increase.
The mean of return will augment too. With the higher budget, there would be more frequent 
observations around the mean of return.
'''


