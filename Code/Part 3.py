"""
An investor will start with a certain given budget, an investor mode (aggressive, defensive or mixed)
A defensive investor
        will only invest in short and long term bonds.
        He will randomly invest (50-50) in long and short term bonds
        as long as he has at least enough money to pay for a short term bond.
An aggressive investor
        will only invest in stocks.
        First a randomly chosen stock will be selected.
        Then, depending on the investor remaining budget,
        a random amount of stocks between 0 and the maximum of stocks that investor would be able to buy is bought.
        This is repeated until he has less than 100$ available.
A mixed investor
        will invest first have a 25-75 chance of buying a bond or stocks.
        If he buys a bond it is distributed 50-50 in long and short.
        If he buys a stock, the same rules apply as the aggressive investor.
        In this case the mixed investor will keep on investing until the has not enough to buy the short term bond.

Model 500 aggressive investors, 500 mixed investors and 500 defensive investors with a starting budget 5000.
What do you conclusions can you draw from this?
For now you can assume bonds are kept even after their minimum term.
"""

# Import packages
import os
import easygui as g
import matplotlib.pyplot as plt
import pandas as pd
import yfinance as yf
import random
import numpy as np

# A review of bond information
bond = ["ShortTermBond", "LongTermBond"]

# Short term bond information
ShortTermMinimumTerm = 2
ShortTermMinimumAmount = 250
ShortTermAnnualRate = 0.015

# Long term bond information
LongTermMinimumTerm = 5
LongTermMinimumAmount = 1000
LongTermAnnualRate = 0.03

# A review of stock information
stock = ["FDX", "GOOGL", "IBM", "KO", "MS", "NOK", "XOM"]

# Extract date data from yahoo finance
dataset = yf.download('FDX', '2016-09-02', '2021-01-01')
date = list(dataset.index)

# Import datasets (stocks)
FDX = os.path.abspath('..\Data\FDX.csv')
FDXdata = pd.read_csv(FDX, sep=",")

GOOGL = os.path.abspath("..\Data\GOOGL.csv")
GOOGLdata = pd.read_csv(GOOGL, sep=",")

IBM = os.path.abspath("..\Data\IBM.csv")
IBMdata = pd.read_csv(IBM, sep=",")

KO = os.path.abspath("..\Data\KO.csv")
KOdata = pd.read_csv(KO, sep=",")

MS = os.path.abspath("..\Data\MS.csv")
MSdata = pd.read_csv(MS, sep=",")

NOK = os.path.abspath("..\Data\\NOK.csv")
NOKdata = pd.read_csv(NOK, sep=",")

XOM = os.path.abspath("..\Data\XOM.csv")
XOMdata = pd.read_csv(XOM, sep=",")

# Add the date column to each dataset
FDXdata['date'] = date
GOOGLdata["date"] = date
IBMdata["date"] = date
KOdata["date"] = date
MSdata["date"] = date
NOKdata["date"] = date
XOMdata["date"] = date

# Assumptions
start_date = "2016-09-01" # the date at which the investor will buy the stock
end_date = "2020-12-31" # the date at which the investor will sell the stock
TotalReturn_Bond = 0
TotalReturn_Stock = 0

# Investor input an investor mode
InvestorMode = g.choicebox(msg="Choose your investment mode",
                           choices=["Defensive investor", "Aggressive investor", "Mixed investor"],
                           title="Investor mode")

if InvestorMode == "Defensive investor":
    for i in range(500):
        Budget = 5000
        ShortTermBondCount = 0 # initiate a count variable for short term bond
        LongTermBondCount = 0 # initiate a count variable for long term bond
        while Budget >= 1000:  # loop until budget is smaller than 1000
            BondType = np.random.choice(a=bond, size=1, replace=False, p=[0.5, 0.5]) # 50-50 in short long
            if BondType == "ShortTermBond":
                Budget = Budget - ShortTermMinimumAmount
                ShortTermBondCount += 1
            elif BondType == "LongTermBond":
                Budget = Budget - LongTermMinimumAmount
                LongTermBondCount += 1
        else:  # if the remaining budget is below 1000, then the investor can only buy short term bond
            amount = int(Budget // ShortTermMinimumAmount)
            ShortTermBondCount = ShortTermBondCount + amount

        # Short term bond return
        if ShortTermBondCount != 0:
            ShortTermBondValue = ShortTermBondCount * ShortTermMinimumAmount
            ShortTermBondInterest = (1 + ShortTermAnnualRate) ** ShortTermMinimumTerm
            ShortTermBondReturn = ((ShortTermBondValue * ShortTermBondInterest) - ShortTermBondValue) / ShortTermBondValue

        # Long term bond return
        if LongTermBondCount != 0:
            LongTermBondValue = LongTermBondCount * LongTermMinimumAmount
            LongTermBondInterest = (1 + LongTermAnnualRate) ** LongTermMinimumTerm
            LongTermBondReturn = ((LongTermBondValue * LongTermBondInterest) - LongTermBondValue) / LongTermBondValue

        # Total return for defensive investor
        if ShortTermBondCount != 0 and LongTermBondCount != 0:
            TotalReturn = ShortTermBondReturn + LongTermBondReturn
            TotalReturn_Bond = TotalReturn_Bond + TotalReturn

    # Average return of the 500 defensive investors
    AverageReturn_Bond = TotalReturn_Bond / 500
    print("The average return of the 500 defensive investors is {:.2%}".format(AverageReturn_Bond))

elif InvestorMode == "Aggressive investor":
    for i in range(500):
        Budget = 5000
        TotalStockBought_start = 0
        TotalStockBought_end = 0
        while Budget >= 100:
            # Randomly choose stock
            StockType = np.random.choice(a=stock, size=1, replace=False, p=None)
            if StockType == "FDX":
                price_start = float(FDXdata.loc[FDXdata["date"] == start_date, "high"].iloc[0])
                amount = (Budget // price_start)
                StockBought = random.randint(0, amount) # random amount of stocks between 0 and the maximum of stocks
                Budget = Budget - (StockBought * price_start)
                TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                price_end = float(FDXdata.loc[FDXdata["date"] == end_date, "high"].iloc[0])
                TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
            elif StockType == "GOOGL":
                price_start = float(GOOGLdata.loc[GOOGLdata["date"] == start_date, "high"].iloc[0])
                amount = (Budget // price_start)
                StockBought = random.randint(0, amount)
                Budget = Budget - (StockBought * price_start)
                TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                price_end = float(GOOGLdata.loc[GOOGLdata["date"] == end_date, "high"].iloc[0])
                TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
            elif StockType == "IBM":
                price_start = float(IBMdata.loc[IBMdata["date"] == start_date, "high"].iloc[0])
                amount = (Budget // price_start)
                StockBought = random.randint(0, amount)
                Budget = Budget - (StockBought * price_start)
                TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                price_end = float(IBMdata.loc[IBMdata["date"] == end_date, "high"].iloc[0])
                TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
            elif StockType == "KO":
                price_start = float(KOdata.loc[KOdata["date"] == start_date, "high"].iloc[0])
                amount = (Budget // price_start)
                StockBought = random.randint(0, amount)
                Budget = Budget - (StockBought * price_start)
                TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                price_end = float(KOdata.loc[KOdata["date"] == end_date, "high"].iloc[0])
                TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
            elif StockType == "MS":
                price_start = float(MSdata.loc[MSdata["date"] == start_date, "high"].iloc[0])
                amount = (Budget // price_start)
                StockBought = random.randint(0, amount)
                Budget = Budget - (StockBought * price_start)
                TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                price_end = float(MSdata.loc[MSdata["date"] == end_date, "high"].iloc[0])
                TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
            elif StockType == "NOK":
                price_start = float(NOKdata.loc[NOKdata["date"] == start_date, "high"].iloc[0])
                amount = (Budget // price_start)
                StockBought = random.randint(0, amount)
                Budget = Budget - (StockBought * price_start)
                TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                price_end = float(NOKdata.loc[NOKdata["date"] == end_date, "high"].iloc[0])
                TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
            elif StockType == "XOM":
                price_start = float(XOMdata.loc[XOMdata["date"] == start_date, "high"].iloc[0])
                amount = (Budget // price_start)
                StockBought = random.randint(0, amount)
                Budget = Budget - (StockBought * price_start)
                TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                price_end = float(XOMdata.loc[XOMdata["date"] == end_date, "high"].iloc[0])
                TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)

        # Investment return for aggressive investor
        if TotalStockBought_start != 0 and TotalStockBought_end != 0:
            TotalReturn = (TotalStockBought_end - TotalStockBought_start) / TotalStockBought_start
            TotalReturn_Stock= TotalReturn_Stock + TotalReturn

    # Average return of the 500 aggressive investors
    AverageReturn_Stock = TotalReturn_Stock / 500
    print("The average return of the 500 aggressive investors is {:.2%}".format(AverageReturn_Stock))

elif InvestorMode == "Mixed investor":
    for i in range(500):
        Budget = 5000
        ShortTermBondCount = 0
        LongTermBondCount = 0
        TotalStockBought_start = 0
        TotalStockBought_end = 0
        while Budget >= 250:  # loop until budget is smaller than 250
            # 25-75 chance of buying a bond or stocks
            InvestmentType = np.random.choice(["Bond", "Stock"], size=1, replace=False, p=[0.25, 0.75])
            if InvestmentType == "Bond":
                BondType = np.random.choice(a=["ShortTermBond", "LongTermBond"], size=1, replace=False, p=[0.5, 0.5])
                if BondType == "ShortTermBond":
                    Budget = Budget - ShortTermMinimumAmount
                    ShortTermBondCount += 1
                elif BondType == "LongTermBond":
                    Budget = Budget - LongTermMinimumAmount
                    LongTermBondCount += 1
            elif InvestmentType == "Stock":
                StockType = np.random.choice(a=stock, size=1, replace=False, p=None)
                if StockType == "FDX":
                    price_start = float(FDXdata.loc[FDXdata["date"] == start_date, "high"].iloc[0])
                    amount = (Budget // price_start)
                    StockBought = random.randint(0, amount)
                    Budget = Budget - (StockBought * price_start)
                    TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                    price_end = float(FDXdata.loc[FDXdata["date"] == end_date, "high"].iloc[0])
                    TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
                elif StockType == "GOOGL":
                    price_start = float(GOOGLdata.loc[GOOGLdata["date"] == start_date, "high"].iloc[0])
                    amount = (Budget // price_start)
                    StockBought = random.randint(0, amount)
                    Budget = Budget - (StockBought * price_start)
                    TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                    price_end = float(GOOGLdata.loc[GOOGLdata["date"] == end_date, "high"].iloc[0])
                    TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
                elif StockType == "IBM":
                    price_start = float(IBMdata.loc[IBMdata["date"] == start_date, "high"].iloc[0])
                    amount = (Budget // price_start)
                    StockBought = random.randint(0, amount)
                    Budget = Budget - (StockBought * price_start)
                    TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                    price_end = float(IBMdata.loc[IBMdata["date"] == end_date, "high"].iloc[0])
                    TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
                elif StockType == "KO":
                    price_start = float(KOdata.loc[KOdata["date"] == start_date, "high"].iloc[0])
                    amount = (Budget // price_start)
                    StockBought = random.randint(0, amount)
                    Budget = Budget - (StockBought * price_start)
                    TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                    price_end = float(KOdata.loc[KOdata["date"] == end_date, "high"].iloc[0])
                    TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
                elif StockType == "MS":
                    price_start = float(MSdata.loc[MSdata["date"] == start_date, "high"].iloc[0])
                    amount = (Budget // price_start)
                    StockBought = random.randint(0, amount)
                    Budget = Budget - (StockBought * price_start)
                    TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                    price_end = float(MSdata.loc[MSdata["date"] == end_date, "high"].iloc[0])
                    TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
                elif StockType == "NOK":
                    price_start = float(NOKdata.loc[NOKdata["date"] == start_date, "high"].iloc[0])
                    amount = (Budget // price_start)
                    StockBought = random.randint(0, amount)
                    Budget = Budget - (StockBought * price_start)
                    TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                    price_end = float(NOKdata.loc[NOKdata["date"] == end_date, "high"].iloc[0])
                    TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
                elif StockType == "XOM":
                    price_start = float(XOMdata.loc[XOMdata["date"] == start_date, "high"].iloc[0])
                    amount = (Budget // price_start)
                    StockBought = random.randint(0, amount)
                    Budget = Budget - (StockBought * price_start)
                    TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                    price_end = float(XOMdata.loc[XOMdata["date"] == end_date, "high"].iloc[0])
                    TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)

        # Short term bond return
        if ShortTermBondCount != 0:
            ShortTermBondValue = ShortTermBondCount * ShortTermMinimumAmount
            ShortTermBondInterest = (1 + ShortTermAnnualRate) ** ShortTermMinimumTerm
            ShortTermBondReturn = ((ShortTermBondValue * ShortTermBondInterest) - ShortTermBondValue) / ShortTermBondValue

        # Long term bond return
        if LongTermBondCount != 0:
            LongTermBondValue = LongTermBondCount * LongTermMinimumAmount
            LongTermBondInterest = (1 + LongTermAnnualRate) ** LongTermMinimumTerm
            LongTermBondReturn = ((LongTermBondValue * LongTermBondInterest) - LongTermBondValue) / LongTermBondValue

        # Total bond return
        if ShortTermBondCount != 0 and LongTermBondCount != 0:
            Return_Bond = ShortTermBondReturn + LongTermBondReturn
            TotalReturn_Bond = TotalReturn_Bond + Return_Bond

        # Stock return
        if TotalStockBought_start != 0 and TotalStockBought_end != 0:
            Return_Stock = (TotalStockBought_end - TotalStockBought_start) / TotalStockBought_start
            TotalReturn_Stock = TotalReturn_Stock + Return_Stock

    # Total return for mixed investor
    TotalReturn_Mixed = TotalReturn_Bond + TotalReturn_Stock
    AverageReturn_Mixed = TotalReturn_Mixed / 500
    print("The average return of the 500 mixed investors is {:.2%}".format(AverageReturn_Mixed))

# Conclusions
"""
Based on the code logic above, we tried to run the different investors modes for 300 times respectively, 
and we have the results below. 
  The Defensive investor total return mainly lays in the interval of 17.9%-18.7% (0.179-0.187), 
  and the most frequent total return is around 18.4% (0.184).
  The Aggressive investor total return mainly lays in the interval of 23%-33% (0.23-0.33), 
  and the most frequent total return is around 29% (0.29).
  The Mixed investor total return mainly lays in the interval of 27%-40% (0.27-0.4), 
  and the most frequent total return is around 33% (0.33).
We can draw a conclusion that the defensive portfolio that includes only the bond investment is risk-free, 
and the aggressive portfolio including only stocks has a high return with high risks,
and the mixed mode investors represent portfolios that optimally combine risks and returns. 

To some points, it could explain the Capital Market Line (CML), 
individual investors will hold just the risk-free asset or risky asset or some combinations of 
the bond and the stock portfolios, depending on their risk-aversion. 
As an investor moves up the CML, the overall portfolio risks and returns increase. 
Risk aversed investors will select portfolios close to the risk-free asset, preferring low variance to higher returns. 
Less risk aversed investors will prefer portfolios higher up on the CML, with a higher expected return, but more risks. 
"""



