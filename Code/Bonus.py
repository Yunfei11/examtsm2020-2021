"""
BONUS
What was on average the best year for an aggressive investor (always going from 01/01 to 31/12)
Model the same number of investors but now have starting budget randomly
from a normal distribution centered around 20000 with a standard deviation of 5000
(keep in mind the tails to the lower side).
What was the best stock to have in 2007?
"""


# BONUS 1
# What was on average the best year for an aggressive investor
'''
From Yearly returns graph in the part 4.1, we could conclude that the best year of
for aggressive investor is 2020. Although the world is experiencing the COVID-19 in 2020 and 
the economic of the world is struggling, the stock market is booming during 2020.
Because the Fed (the Federal Reserve) implement QE (quantitative easing) policy,
and also interest rates have continued to fall. As a consequence,
low interest rates boost the prices of many kinds of assets, including stocks. 
In addition, the stock on the provided list above, are mainly large transnational corporation.
who tend to have global footprints. It could diversify risk.

'''


# BONUS 2
# Random budget from a normal distribution centered around 20000 with a standard deviation of 5000

# We change the parameter of Budget from 5000 to np.random.normal(20000, 5000, 1)

# Import packages
import os
import easygui as g
import matplotlib.pyplot as plt
import pandas as pd
import yfinance as yf
import random
import numpy as np

# A review of bond information
bond = ["ShortTermBond", "LongTermBond"]

# Short term bond information
ShortTermMinimumTerm = 2
ShortTermMinimumAmount = 250
ShortTermAnnualRate = 0.015

# Long term bond information
LongTermMinimumTerm = 5
LongTermMinimumAmount = 1000
LongTermAnnualRate = 0.03

# A review of stock information
stock = ["FDX", "GOOGL", "IBM", "KO", "MS", "NOK", "XOM"]

# Extract date data from yahoo finance
dataset = yf.download('FDX', '2016-09-02', '2021-01-01')
date = list(dataset.index)

# Import datasets (stocks)
FDX = os.path.abspath('..\Data\FDX.csv')
FDXdata = pd.read_csv(FDX, sep=",")

GOOGL = os.path.abspath("..\Data\GOOGL.csv")
GOOGLdata = pd.read_csv(GOOGL, sep=",")

IBM = os.path.abspath("..\Data\IBM.csv")
IBMdata = pd.read_csv(IBM, sep=",")

KO = os.path.abspath("..\Data\KO.csv")
KOdata = pd.read_csv(KO, sep=",")

MS = os.path.abspath("..\Data\MS.csv")
MSdata = pd.read_csv(MS, sep=",")

NOK = os.path.abspath("..\Data\\NOK.csv")
NOKdata = pd.read_csv(NOK, sep=",")

XOM = os.path.abspath("..\Data\XOM.csv")
XOMdata = pd.read_csv(XOM, sep=",")

# Add the date column to each dataset
FDXdata['date'] = date
GOOGLdata["date"] = date
IBMdata["date"] = date
KOdata["date"] = date
MSdata["date"] = date
NOKdata["date"] = date
XOMdata["date"] = date

# Assumptions
start_date = "2016-09-01" # the date at which the investor will buy the stock
end_date = "2020-12-31" # the date at which the investor will sell the stock
TotalReturn_Stock = 0
TotalReturn_Bond = 0

# Investor input an investor mode
InvestorMode = g.choicebox(msg="Choose your investment mode",
                           choices=["Defensive investor", "Aggressive investor", "Mixed investor"],
                           title="Investor mode")

if InvestorMode == "Defensive investor":
    for i in range(500):
        Budget = np.random.normal(20000, 5000, 1)
        ShortTermBondCount = 0
        LongTermBondCount = 0
        while Budget >= 1000:  # loop until budget is smaller than 1000
            BondType = np.random.choice(a=bond, size=1, replace=False, p=[0.5, 0.5])
            if BondType == "ShortTermBond":
                Budget = Budget - ShortTermMinimumAmount
                ShortTermBondCount += 1
            elif BondType == "LongTermBond":
                Budget = Budget - LongTermMinimumAmount
                LongTermBondCount += 1
        else:  # if the remaining budget is below 1000, then the investor can only buy short term bond
            amount = int(Budget // ShortTermMinimumAmount)
            ShortTermBondCount = ShortTermBondCount + amount

        # Short term bond return
        if ShortTermBondCount != 0:
            ShortTermBondValue = ShortTermBondCount * ShortTermMinimumAmount
            ShortTermBondInterest = (1 + ShortTermAnnualRate) ** ShortTermMinimumTerm
            ShortTermBondReturn = ((ShortTermBondValue * ShortTermBondInterest) - ShortTermBondValue) / ShortTermBondValue

        # Long term bond return
        if LongTermBondCount != 0:
            LongTermBondValue = LongTermBondCount * LongTermMinimumAmount
            LongTermBondInterest = (1 + LongTermAnnualRate) ** LongTermMinimumTerm
            LongTermBondReturn = ((LongTermBondValue * LongTermBondInterest) - LongTermBondValue) / LongTermBondValue

        # Total return for defensive investor
        if ShortTermBondCount != 0 and LongTermBondCount != 0:
            TotalReturn = ShortTermBondReturn + LongTermBondReturn
            TotalReturn_Bond = TotalReturn_Bond + TotalReturn

    # Average return of the 500 defensive investors
    AverageReturn_Bond = TotalReturn_Bond / 500
    print("The average return of the 500 defensive investors is {:.2%}".format(AverageReturn_Bond))

elif InvestorMode == "Aggressive investor":
    for i in range(500):
        Budget = np.random.normal(20000, 5000, 1)
        TotalStockBought_start = 0
        TotalStockBought_end = 0
        while Budget >= 100:
            # Randomly choose stock
            StockType = np.random.choice(a=stock, size=1, replace=False, p=None)
            if StockType == "FDX":
                price_start = float(FDXdata.loc[FDXdata["date"] == start_date, "high"].iloc[0])
                amount = (Budget // price_start)
                StockBought = random.randint(0, amount)
                Budget = Budget - (StockBought * price_start)
                TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                price_end = float(FDXdata.loc[FDXdata["date"] == end_date, "high"].iloc[0])
                TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
            elif StockType == "GOOGL":
                price_start = float(GOOGLdata.loc[GOOGLdata["date"] == start_date, "high"].iloc[0])
                amount = (Budget // price_start)
                StockBought = random.randint(0, amount)
                Budget = Budget - (StockBought * price_start)
                TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                price_end = float(GOOGLdata.loc[GOOGLdata["date"] == end_date, "high"].iloc[0])
                TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
            elif StockType == "IBM":
                price_start = float(IBMdata.loc[IBMdata["date"] == start_date, "high"].iloc[0])
                amount = (Budget // price_start)
                StockBought = random.randint(0, amount)
                Budget = Budget - (StockBought * price_start)
                TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                price_end = float(IBMdata.loc[IBMdata["date"] == end_date, "high"].iloc[0])
                TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
            elif StockType == "KO":
                price_start = float(KOdata.loc[KOdata["date"] == start_date, "high"].iloc[0])
                amount = (Budget // price_start)
                StockBought = random.randint(0, amount)
                Budget = Budget - (StockBought * price_start)
                TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                price_end = float(KOdata.loc[KOdata["date"] == end_date, "high"].iloc[0])
                TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
            elif StockType == "MS":
                price_start = float(MSdata.loc[MSdata["date"] == start_date, "high"].iloc[0])
                amount = (Budget // price_start)
                StockBought = random.randint(0, amount)
                Budget = Budget - (StockBought * price_start)
                TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                price_end = float(MSdata.loc[MSdata["date"] == end_date, "high"].iloc[0])
                TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
            elif StockType == "NOK":
                price_start = float(NOKdata.loc[NOKdata["date"] == start_date, "high"].iloc[0])
                amount = (Budget // price_start)
                StockBought = random.randint(0, amount)
                Budget = Budget - (StockBought * price_start)
                TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                price_end = float(NOKdata.loc[NOKdata["date"] == end_date, "high"].iloc[0])
                TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
            elif StockType == "XOM":
                price_start = float(XOMdata.loc[XOMdata["date"] == start_date, "high"].iloc[0])
                amount = (Budget // price_start)
                StockBought = random.randint(0, amount)
                Budget = Budget - (StockBought * price_start)
                TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                price_end = float(XOMdata.loc[XOMdata["date"] == end_date, "high"].iloc[0])
                TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)

        # Investment return for aggressive investor
        TotalReturn = (TotalStockBought_end - TotalStockBought_start) / TotalStockBought_start
        TotalReturn_Stock= TotalReturn_Stock + TotalReturn

    # Average return of the 500 aggressive investors
    AverageReturn_Stock = TotalReturn_Stock / 500
    print("The average return of the 500 aggressive investors is {:.2%}".format(AverageReturn_Stock))

elif InvestorMode == "Mixed investor":
    for i in range(500):
        Budget = np.random.normal(20000, 5000, 1)
        ShortTermBondCount = 0
        LongTermBondCount = 0
        TotalStockBought_start = 0
        TotalStockBought_end = 0
        while Budget >= 250:  # loop until budget is smaller than 250
            # 25-75 chance of buying a bond or stocks
            InvestmentType = np.random.choice(["Bond", "Stock"], size=1, replace=False, p=[0.25, 0.75])
            if InvestmentType == "Bond":
                BondType = np.random.choice(a=["ShortTermBond", "LongTermBond"], size=1, replace=False, p=[0.5, 0.5])
                if BondType == "ShortTermBond":
                    Budget = Budget - ShortTermMinimumAmount
                    ShortTermBondCount += 1
                elif BondType == "LongTermBond":
                    Budget = Budget - LongTermMinimumAmount
                    LongTermBondCount += 1
            elif InvestmentType == "Stock":
                StockType = np.random.choice(a=stock, size=1, replace=False, p=None)
                if StockType == "FDX":
                    price_start = float(FDXdata.loc[FDXdata["date"] == start_date, "high"].iloc[0])
                    amount = (Budget // price_start)
                    StockBought = random.randint(0, amount)
                    Budget = Budget - (StockBought * price_start)
                    TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                    price_end = float(FDXdata.loc[FDXdata["date"] == end_date, "high"].iloc[0])
                    TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
                elif StockType == "GOOGL":
                    price_start = float(GOOGLdata.loc[GOOGLdata["date"] == start_date, "high"].iloc[0])
                    amount = (Budget // price_start)
                    StockBought = random.randint(0, amount)
                    Budget = Budget - (StockBought * price_start)
                    TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                    price_end = float(GOOGLdata.loc[GOOGLdata["date"] == end_date, "high"].iloc[0])
                    TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
                elif StockType == "IBM":
                    price_start = float(IBMdata.loc[IBMdata["date"] == start_date, "high"].iloc[0])
                    amount = (Budget // price_start)
                    StockBought = random.randint(0, amount)
                    Budget = Budget - (StockBought * price_start)
                    TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                    price_end = float(IBMdata.loc[IBMdata["date"] == end_date, "high"].iloc[0])
                    TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
                elif StockType == "KO":
                    price_start = float(KOdata.loc[KOdata["date"] == start_date, "high"].iloc[0])
                    amount = (Budget // price_start)
                    StockBought = random.randint(0, amount)
                    Budget = Budget - (StockBought * price_start)
                    TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                    price_end = float(KOdata.loc[KOdata["date"] == end_date, "high"].iloc[0])
                    TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
                elif StockType == "MS":
                    price_start = float(MSdata.loc[MSdata["date"] == start_date, "high"].iloc[0])
                    amount = (Budget // price_start)
                    StockBought = random.randint(0, amount)
                    Budget = Budget - (StockBought * price_start)
                    TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                    price_end = float(MSdata.loc[MSdata["date"] == end_date, "high"].iloc[0])
                    TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
                elif StockType == "NOK":
                    price_start = float(NOKdata.loc[NOKdata["date"] == start_date, "high"].iloc[0])
                    amount = (Budget // price_start)
                    StockBought = random.randint(0, amount)
                    Budget = Budget - (StockBought * price_start)
                    TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                    price_end = float(NOKdata.loc[NOKdata["date"] == end_date, "high"].iloc[0])
                    TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)
                elif StockType == "XOM":
                    price_start = float(XOMdata.loc[XOMdata["date"] == start_date, "high"].iloc[0])
                    amount = (Budget // price_start)
                    StockBought = random.randint(0, amount)
                    Budget = Budget - (StockBought * price_start)
                    TotalStockBought_start = TotalStockBought_start + (StockBought * price_start)
                    price_end = float(XOMdata.loc[XOMdata["date"] == end_date, "high"].iloc[0])
                    TotalStockBought_end = TotalStockBought_end + (StockBought * price_end)

        # Short term bond return
        if ShortTermBondCount != 0:
            ShortTermBondValue = ShortTermBondCount * ShortTermMinimumAmount
            ShortTermBondInterest = (1 + ShortTermAnnualRate) ** ShortTermMinimumTerm
            ShortTermBondReturn = ((ShortTermBondValue * ShortTermBondInterest) - ShortTermBondValue) / ShortTermBondValue

        # Long term bond return
        if LongTermBondCount != 0:
            LongTermBondValue = LongTermBondCount * LongTermMinimumAmount
            LongTermBondInterest = (1 + LongTermAnnualRate) ** LongTermMinimumTerm
            LongTermBondReturn = ((LongTermBondValue * LongTermBondInterest) - LongTermBondValue) / LongTermBondValue

        # Total bond return
        if ShortTermBondCount != 0 and LongTermBondCount != 0:
            Return_Bond = ShortTermBondReturn + LongTermBondReturn
            TotalReturn_Bond = TotalReturn_Bond + Return_Bond

        # Stock return
        Return_Stock = (TotalStockBought_end - TotalStockBought_start) / TotalStockBought_start
        TotalReturn_Stock = TotalReturn_Stock + Return_Stock

    # Total return for mixed investor
    TotalReturn_Mixed = TotalReturn_Bond + TotalReturn_Stock
    AverageReturn_Mixed = TotalReturn_Mixed / 500
    print("The average return of the 500 mixed investors is {:.2%}".format(AverageReturn_Mixed))


# BONUS 3 What was the best stock to have in 2007?
"""
From our point of view, in 2007, it was the most glorious moments for NOKIA. 
Therefore, we conjecture the best stock among the selected stocks in 2007 is NOKIA.
The next part of code is the evidence from yahoo finance.
"""
import matplotlib.pyplot as plt
import yfinance as yf

stock = ["FDX", "GOOGL", "IBM", "KO", "MS", "NOK", "XOM"]
start_date = '2007-01-01'
end_date = '2007-12-31'
returns = []

FDXdata = yf.download('FDX', start_date, end_date)
price_start = FDXdata["Close"][0]
price_end = FDXdata["Close"][-1]
FDXreturn = (price_end - price_start) / price_start
returns.append(FDXreturn)

GOOGLdata = yf.download('GOOGL', start_date, end_date)
price_start = GOOGLdata["Close"][0]
price_end = GOOGLdata["Close"][-1]
GOOGLreturn = (price_end - price_start) / price_start
returns.append(GOOGLreturn)

IBMdata = yf.download('IBM', start_date, end_date)
price_start = IBMdata["Close"][0]
price_end = IBMdata["Close"][-1]
IBMreturn = (price_end - price_start) / price_start
returns.append(IBMreturn)

KOdata = yf.download('KO', start_date, end_date)
price_start = KOdata["Close"][0]
price_end = KOdata["Close"][-1]
KOreturn = (price_end - price_start) / price_start
returns.append(KOreturn)

MSdata = yf.download('MS', start_date, end_date)
price_start = MSdata["Close"][0]
price_end = MSdata["Close"][-1]
MSreturn = (price_end - price_start) / price_start
returns.append(MSreturn)

NOKdata = yf.download('NOK', start_date, end_date)
price_start = NOKdata["Close"][0]
price_end = NOKdata["Close"][-1]
NOKreturn = (price_end - price_start) / price_start
returns.append(NOKreturn)


XOMdata = yf.download('XOM', start_date, end_date)
price_start = XOMdata["Close"][0]
price_end = XOMdata["Close"][-1]
XOMreturn = (price_end - price_start) / price_start
returns.append(XOMreturn)


plt.bar(stock, returns,color="lightgreen")
plt.xlabel("Stocks", fontsize=10)
plt.ylabel("Returns", fontsize=10)
plt.title("Stock performance in 2007", fontsize=12)
plt.grid(True)
plt.show()