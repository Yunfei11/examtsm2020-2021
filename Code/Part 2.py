"""
In this part we will start by modelling the stocks. You are free to either use the data provided in the CSV files .
The names of the stocks in scope are : 'FDX', 'GOOGL','XOM', 'KO', 'NOK', 'MS', 'IBM‘ from 09/01/2016 to 01/01/2021
Stocks have the following criteria:
                    a term during which the investment will be done,
                    a certain invested amount,
                    a stockname (see above),
                    the number of stocks bought
                    and the date on which they we’re bought (notice only business days are possible).
We will assume that we can buy and sell them without additional transaction fees.
Use the ’High’ column as the price of the stocks.
It should be possible to (given the start and end date) to get the price of the stock and return on investment.
If a start and end date is not a business day take the closest possible business day before the the given date.
Make a plot of all stocks for the entire period
"""

# Import packages
import datetime
import os
import easygui as g
import matplotlib.pyplot as plt
import pandas as pd
import yfinance as yf

# Extract date data from yahoo finance
dataset = yf.download('FDX', '2016-09-02', '2021-01-01')
date = list(dataset.index)

# Import datasets (stocks)
FDX = os.path.abspath('..\Data\FDX.csv')
FDXdata = pd.read_csv(FDX, sep=",")

GOOGL = os.path.abspath("..\Data\GOOGL.csv")
GOOGLdata = pd.read_csv(GOOGL, sep=",")

IBM = os.path.abspath("..\Data\IBM.csv")
IBMdata = pd.read_csv(IBM, sep=",")

KO = os.path.abspath("..\Data\KO.csv")
KOdata = pd.read_csv(KO, sep=",")

MS = os.path.abspath("..\Data\MS.csv")
MSdata = pd.read_csv(MS, sep=",")

NOK = os.path.abspath("..\Data\\NOK.csv")
NOKdata = pd.read_csv(NOK, sep=",")

XOM = os.path.abspath("..\Data\XOM.csv")
XOMdata = pd.read_csv(XOM, sep=",")

# Add the date column to each dataset
FDXdata['date'] = date
GOOGLdata["date"] = date
IBMdata["date"] = date
KOdata["date"] = date
MSdata["date"] = date
NOKdata["date"] = date
XOMdata["date"] = date

# User can now input the stock, the start date, the end date, and the number of stock bought
stock = g.choicebox(msg="Choose your invested stock", choices=["FDX", "GOOGL", "IBM", "KO", "MS", "NOK", "XOM"])
startdate = g.enterbox(msg="Please enter the start date between 2016-09-01 and 2021-01-01 (yyyy-mm-dd)",
                       title=" Start date")
enddate = g.enterbox(msg="Please enter the start date between 2016-09-01 and 2021-01-01 (yyyy-mm-dd)",
                     title=" Start date")
number_stocks = g.enterbox(msg="Please enter the number of " + stock + " bought", title=" Number of stock bought")

# The inputted start date and end date are string type,
# In order to compare and find the relevant data in database, we reformat the input data as date
# And separate into year, month and day for next step
startdate = datetime.datetime.strptime(startdate, "%Y-%m-%d")
startdate_separated = datetime.date(startdate.year, startdate.month, startdate.day)
enddate = datetime.datetime.strptime(enddate, "%Y-%m-%d")
enddate_separated = datetime.date(enddate.year, enddate.month, enddate.day)

# Check if the start date and the end date is NYSE business day
# if the inputted date is holiday or weekend, we take the latest business day as the start day or the end day
# There is no consecutive two-day holiday in NYSE,
# Therefore we can assume that even if the inputted date is holiday, the business day is still in the last 3 day
day1 = datetime.timedelta(1)
day2 = datetime.timedelta(2)
day3 = datetime.timedelta(3)

if startdate in date:
    start_date = startdate.date()
else:
    if startdate_separated - day1 in date:
        start_date = startdate_separated - day1
    elif (startdate_separated - day1) not in date and (startdate_separated - day2) in date:
        start_date = startdate_separated - day2
    elif (startdate_separated - day1) not in date and (startdate_separated - day2) not in date and (
            startdate_separated - day3) in date:
        start_date = startdate_separated - day3

if enddate in date:
    end_date = enddate.date()
else:
    if enddate_separated - day1 in date:
        end_date = enddate_separated - day1
    elif (enddate_separated - day1) not in date and (enddate_separated - day2) in date:
        end_date = enddate_separated - day2
    elif (enddate_separated - day1) not in date and (enddate_separated - day2) not in date and (
            enddate_separated - day3) in date:
        end_date = enddate_separated - day3

# Format the dates into string for next step
start_date = str(start_date)
end_date = str(end_date)

# Find the corresponding prices from the inputted dates
if stock == "FDX":
    price_start = float(FDXdata.loc[FDXdata["date"] == start_date, "high"].iloc[0])
    price_end = float(FDXdata.loc[FDXdata["date"] == end_date, "high"].iloc[0])
elif stock == "GOOGL":
    price_start = float(GOOGLdata.loc[GOOGLdata["date"] == start_date, "high"].iloc[0])
    price_end = float(GOOGLdata.loc[GOOGLdata["date"] == end_date, "high"].iloc[0])
elif stock == "IBM":
    price_start = float(IBMdata.loc[IBMdata["date"] == start_date, "high"].iloc[0])
    price_end = float(IBMdata.loc[IBMdata["date"] == end_date, "high"].iloc[0])
elif stock == "KO":
    price_start = float(KOdata.loc[KOdata["date"] == start_date, "high"].iloc[0])
    price_end = float(KOdata.loc[KOdata["date"] == end_date, "high"].iloc[0])
elif stock == "MS":
    price_start = float(MSdata.loc[MSdata["date"] == start_date, "high"].iloc[0])
    price_end = float(MSdata.loc[MSdata["date"] == end_date, "high"].iloc[0])
elif stock == "NOK":
    price_start = float(NOKdata.loc[NOKdata["date"] == start_date, "high"].iloc[0])
    price_end = float(NOKdata.loc[NOKdata["date"] == end_date, "high"].iloc[0])
elif stock == "XOM":
    price_start = float(XOMdata.loc[XOMdata["date"] == start_date, "high"].iloc[0])
    price_end = float(XOMdata.loc[XOMdata["date"] == end_date, "high"].iloc[0])

# Calculate the stock return and investment return
stock_return = (price_end - price_start) / price_start
investment_return = ((price_end - price_start) / price_start) * float(number_stocks)
print("The return of " + stock + " is {:.2f}".format(stock_return * 100) + "%.")
print("The total return of your "+ number_stocks + " shares " + stock + " is {:.2f}".format(investment_return) + ".")


# Plot of all stocks for the entire period
plt.plot(date, FDXdata["high"], label='FDX')
plt.plot(date, GOOGLdata["high"], label='GOOGL')
plt.plot(date, IBMdata["high"], label='IBM')
plt.plot(date, KOdata["high"], label='KO')
plt.plot(date, MSdata["high"], label='MS')
plt.plot(date, NOKdata["high"], label='NOK')
plt.plot(date, XOMdata["high"], label='XOM')
plt.xlabel("Date")
plt.ylabel("Prices")
plt.title("Plot of all stocks")
plt.legend()
plt.grid(True)
plt.show()
