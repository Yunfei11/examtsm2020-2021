# Exam Project

This project aims to conduct simulations of different investing strategies with the three types of investors: defensive, aggressive, mixed. 
Different scenarios have been proposed and simulated, and their results can be shown according to the parameters that the user wishes to enter. 
There are 5 parts in this project: Part 1 (short-term and long-term bond), Part 2 (stocks), Part 3 (investors), Part 4 (simulations), Bonus part. 
The following sections will now present in details the simulations conducted in each part and what parameters the user are invited to enter.  

## Getting Started

The following instructions will provide a preliminary understanding of our project to facilitate the utilization of our program. 

### Prerequisites

Packages you need to install and import

```
import os
import random
import QuantLib as ql
import easygui as g
import numpy as np
import matplotlib.pyplot as plt
import datetime
import pandas as pd
import yfinance as yf
```

## Introduction

A step by step and part by part series of examples that help you to understand our program.

### Part 1

There are two Bonds on the fixed income market:
* Short term bonds have a minimum term of 2 years, a minimum amount of 250 and a yearly interest of 1.5%.
* Long term bonds have a minimum term of 5 years, a minimum amount of 1000 and a yearly interest of 3%.

As a user, he could input the type of bond and a certain investment duration. 
The program will then calculate the compounded interests for the investor and will show the return of the minimum allowed invested amount for both short-term and long-term bond over 50 years.

### Part 2

We have 7 stocks on the stock market ('FDX', 'GOOGL','XOM', 'KO', 'NOK', 'MS', 'IBM‘).

From 09/01/2016 to 01/01/2021, user can input the stock name, the start and end date, and the number of stock bought.
We assume that there's no additional transaction fees and investors can buy and sell always at the high price of a business day.
If the date that investor inputs is not a business day, we will then take the closest business day before the inputted date. 
As a result, the user will get the information about the rate of return and the totol return of his invested stock. 

At last, we would show the plot of all stocks for the entire period as a reference.

### Part 3

Now, the financial market is the combination of 2 bonds and 7 stocks based on above information.
We consider that each investor has a different investor mode ("Defensive investor", "Aggressive investor", "Mixed investor").

* Defensive investor
     * Only invests in short and long-term bonds.
     * Randomly invests 50% in short-term bonds and 50% in and long-term bonds.
     * Invests until there is no money left for another short-term bond.
    
* Aggressive investor
     * Only invests in stocks.
     * Invests in equal possibilities of each stock.
     * Invests a random amount of stocks between 0 and the maximum of stocks that investor would be able to buy. 
     * Invests until the budget is less than 100.

* Mixed investor
     * Invests with 25% probability on bond and 75% probability on stocks.
         * If he buys a bond, distributed 50% in short-term bonds and 50% in long-term bonds.
         * If he buys a stock, equal possibility on each stock.
     * Invests until there is no money left for another short-term bond.

The investor will start to invest on 01/09/2016 and withdraw the investment on 31/12/2020.
This scenario comprises 500 aggressive investors, 500 mixed investors and 500 defensive investors with a starting budget of 5000.
User is invited to choose an investor mode, this program will then automatically calculate the average return of the 500 investors based on the investor type selected.

### Part 4_Simulations

We have 4 different sections in this part. 
All these sections are based on the information below:

  1. The two bonds on the fixed income market:
     * Short-term bonds have a minimum term of 2 years, a minimum amount of 250 and a yearly interest of 1.5%.
     * Long-term bonds have a minimum term of 5 years, a minimum amount of 1000 and a yearly interest of 3%.
  
  2. 7 stocks on the stock market ('FDX', 'GOOGL','XOM', 'KO', 'NOK', 'MS', 'IBM').
  
  3. 3 different investor modes ("Defensive investor", "Aggressive investor", "Mixed investor").
     
  4. A simulation of 500 investors is conducted for each investor mode.

* ### **Part 4.1**

The investor has an initial budget of 5000.
User starts by choosing his investor mode, 
and our program will then give display a plot of the yearly average returns from 2017 to 2020 
as well as a plot of the cumulative returns from 2017 to 2020.

* ### **Part 4.2**

Mixed investors now redistribute their budget (5000) randomly again when their bond periods end, 
which means they will re-invest based after 2 years (minimum term of short term) based on the probability of 25-75 bond vs stock.
As a result, our program will provide the average return of the simulated 500 mixed investors.

Similarly, our program will also provide the average return of the simulated 500 mixed investors if the re-investment is now at the probability of 75-25 bond vs stock.

* ### **Part 4.3**

The results if the budget increases from 5000 to 50000 will be briefly discussed here.
As the overall program remains the same except the change of initial budget to 50000,
we have only written our conclusion in this file. 
If you wish you try, please change the budget variable in Part 3 to 50000.

### Bonus

* ### **BONUS 1**

What was on average the best year for an aggressive investor (always going from 01/01 to 31/12)?

Please refer to the plot in Part 4.1 for an answer.

* ### **BONUS 2**

The 3 types of investors will now be re-modeled with a random normal distributed budget (centered around 20000 with a standard deviation of 5000).

Similarly to Part 3, the investor will start their investments on 01/09/2016 and withdraw on 31/12/2020.
This scenario also comprises 500 aggressive investors, 500 mixed investors and 500 defensive investors.
User is invited to choose an investor mode, our program will then automatically calculate the average return of the 500 investors based on the investor type selected.

* ### **BONUS 3**

To determine the best stock to have in 2007, stock data have been downloaded from Yahoo Finance,
stock returns are calculated and a bar chart are displayed to the user.
This chart shows clearly every stock performance for the year of 2007.

## Notes

Thank you for using our program!

## Authors

* Yunfei LI
* Jialin ZHANG 


