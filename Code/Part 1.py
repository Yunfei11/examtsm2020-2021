"""
In this part we will start by modelling the bonds.
It will be up to you to decide how you structure this given that you will have to incorporate what you do here in the further stages.
Create a new repository on bitbucket and make sure you have setup everything
Bonds have the following characteristics: a term that we’ll invest in them,
a certain amount that will be invested, a minimum price of the bond, a minimum term and yearly interest rate
Short term bonds have a minimum term of 2  years, a minimum amount of 250$ and a yearly interest of 1.5%
Long term bonds have a minimum term of 5  years, a minimum amount of 1000$ and a yearly interest of 3%
Both bonds are compounded yearly and have a way to calculate the compounded interest for a certain time t
Make a plot of the evolution of the investment of the minimum allowed invested amount
for both bonds over a period of 50 years.
"""

# import packages, if not installed, then install first
import QuantLib as ql
import easygui as g
import numpy as np
import matplotlib.pyplot as plt

# Short term bond
ShortTermMinimumTerm = 2
ShortTermMinimumAmount = 250
ShortTermAnnualRate = 0.015

# Long term bond
LongTermMinimumTerm = 5
LongTermMinimumAmount = 1000
LongTermAnnualRate = 0.03

# Set up interest rate
dayCount = ql.ActualActual()  # based on calendar day to calcu interest
compoundType = ql.Compounded  # compound rate
compoundingFrequency = ql.Annual  # yearly paid

ShortTermInterestRate = ql.InterestRate(ShortTermAnnualRate, dayCount, compoundType,
                                        compoundingFrequency)
LongTermInterestRate = ql.InterestRate(LongTermAnnualRate, dayCount, compoundType, compoundingFrequency)

# Allow the user to input the type of bond with a certain time to calculate compounded interest
BondType = g.buttonbox(msg = "Input the type of the bond", title = "", choices = ("Short term bond", "Long term bond"))
if BondType == "Short term bond":
    InterestRate = ShortTermInterestRate
else:
    InterestRate = LongTermInterestRate

time = int(g.enterbox(msg = "Input the term of the bond"))
print(InterestRate.compoundFactor(time))

# Make a plot of the evolution of the investment of the minimum allowed invested amount for both bonds over a period of 50 years
Years = 50
ShortTermValue = list()
LongTermValue = list()
for year in range(Years):
    value = 0
    value += ShortTermMinimumAmount * ((1 + ShortTermAnnualRate) ** year)
    ShortTermValue.append(value)

for year in range(Years):
    value = 0
    value += LongTermMinimumAmount * ((1 + LongTermAnnualRate) ** year)
    LongTermValue.append(value)

time = np.arange(1, Years + 1)
plt.plot(time, ShortTermValue)
plt.xlabel('Years')
plt.ylabel('Bond value')
plt.title('Evolution of the investment of the minimum allowed invested amount for short term bond')
plt.grid(True)
plt.show()

time = np.arange(1, Years + 1)
plt.plot(time, LongTermValue)
plt.xlabel('Years')
plt.ylabel('Bond value')
plt.title('Evolution of the investment of the minimum allowed invested amount for long term bond')
plt.grid(True)
plt.show()


